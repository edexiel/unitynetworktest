using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MenuUI : MonoBehaviour
{
    
    [SerializeField]
    private Panel _panel;

    private GameNetworkHub _gameNetHub;

    public void Start()
    {
        GameObject gameNetHub = GameObject.FindGameObjectWithTag("GameHub");
        Assert.IsNotNull("GameHub not found, start game from startup scene");
        _gameNetHub = gameNetHub.GetComponent<GameNetworkHub>();
    }

    public void OnHostClick()
    {
        _panel.ShowPanel("Hosting game",true, (string ip) => { _gameNetHub.StartHost(ip);});
    }

    public void OnJoinClick()
    {
        _panel.ShowPanel("Joining game",false,(string ip)=>{_gameNetHub.StartClient(ip);});
    }
}
