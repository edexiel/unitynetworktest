using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using MLAPI;
using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour
{
    [SerializeField] private Text _title;
    [SerializeField] private Text _waitingText;
    [SerializeField] private Button _validateButton;
    [SerializeField] private Text _validateText;
    [SerializeField] private InputField _ipInputField;
    [SerializeField] private Dropdown _ipDropdown;
    

    private Action<string> _confirmAction;

    public void ShowPanel(string title, bool host, Action<string> callback)
    {
        _waitingText.gameObject.SetActive(false);
        _confirmAction = callback;

        _title.text = title;
        
        _ipInputField.gameObject.SetActive(!host);
        _ipDropdown.gameObject.SetActive(host);
        
        if (host)
        {
            _validateText.text = "Host";
            _ipDropdown.ClearOptions();
            _ipDropdown.AddOptions(GetLocalIPs());
            //_ip.text = NetworkManager.Singleton.NetworkConfig.NetworkTransport;
            _validateButton.onClick.AddListener(OnValidateHost);
        }
        else
        {
            _ipInputField.readOnly = false;
            _ipInputField.text = "";
            _validateText.text = "Join";
            _validateButton.onClick.AddListener(OnValidateJoin);
        }


        gameObject.SetActive(true);
    }

    private void OnValidateHost()
    {
        _validateButton.gameObject.SetActive(false);
        _waitingText.text = "Waiting on another player";
        _waitingText.gameObject.SetActive(true);
        _confirmAction.Invoke(_ipDropdown.options[_ipDropdown.value].text);
    }

    private void OnValidateJoin()
    {
        _confirmAction.Invoke(string.IsNullOrEmpty(_ipInputField.text)?"127.0.0.1":_ipInputField.text);
    }

    private List<string> GetLocalIPs()
    {
        List<string> ips = new List<string>();

        foreach (var item in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (item.OperationalStatus != OperationalStatus.Up)
                continue;

            foreach (var address in item.GetIPProperties().UnicastAddresses)
            {
                if (address.Address.ToString().Equals("127.0.0.1") ||
                    address.Address.AddressFamily != AddressFamily.InterNetwork)
                    continue;
                
                ips.Add(address.Address.ToString());
            }
        }
        return ips;
    }
}