using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using UnityEngine;

public class GameManager : NetworkBehaviour
{
    [SerializeField] private GameObject Player;

    void Start()
    {
        if (IsHost)
        {
            if (Player == null)
                throw new Exception("Player is not set");
            
            Spawn(ref Player,Vector3.zero,Quaternion.identity);
        }
    }

    private static void Spawn(ref GameObject prefab,Vector3 position, Quaternion rotation )
    {
        GameObject go = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        go.GetComponent<NetworkObject>().Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
