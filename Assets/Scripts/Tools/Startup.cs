using UnityEngine;
using UnityEngine.SceneManagement;


public class Startup : MonoBehaviour
{
    void Start()
    {
        SceneManager.LoadScene("Menu");
    }
}
