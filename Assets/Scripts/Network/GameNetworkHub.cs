using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Serialization.Pooled;
using MLAPI.Transports;
using MLAPI.Transports.UNET;
using UnityEngine;

public class GameNetworkHub : MonoBehaviour
{
    [SerializeField] private GameObject networkingManagerGo;
    public NetworkManager NetManager { get; private set; }

    public event Action NetworkIsReady;
    public event Action ConnectFinished;
    public event Action<ulong,int> ClientSceneChanged;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        NetManager = networkingManagerGo.GetComponent<NetworkManager>();

        NetManager.OnServerStarted += OnNetworkReady;
        NetManager.OnClientConnectedCallback += OnClientConnect;

        RegisterServerMessageHandlers();
    }

    private void OnClientConnect(ulong obj)
    {
        Debug.Log("[hub] Client connected :"+obj);
        //throw new System.NotImplementedException();
        NetworkIsReady?.Invoke();
    }

    private void OnNetworkReady()
    {
        Debug.Log("[hub] Network ready");
        if (NetManager.IsHost)
        {
            ConnectFinished?.Invoke();
        }
        
    }

    public void StartHost(string ip)
    {
        Debug.Log("[hub] Starting host");
        NetManager.StartHost();
    }

    public void StartClient(string ip)
    {
        Debug.Log("[hub] Starting client");
        
        var Transport = (UNetTransport) NetManager.NetworkConfig.NetworkTransport;
        Transport.ConnectAddress = ip;
        NetManager.StartClient();
    }

    public void ClientToServerSceneChanged(int newScene)
    {
        if(NetManager.IsConnectedClient)
        {
            using (var buffer = PooledNetworkBuffer.Get())
            {
                using (var writer = PooledNetworkWriter.Get(buffer))
                {
                    writer.WriteInt32(newScene);
                    MLAPI.Messaging.CustomMessagingManager.SendNamedMessage("ClientToServerSceneChanged", NetManager.ServerClientId, buffer, NetworkChannel.Internal);
                }
            }
        }
    }
    
    private void RegisterServerMessageHandlers()
    {
        MLAPI.Messaging.CustomMessagingManager.RegisterNamedMessageHandler("ClientToServerSceneChanged", (senderClientId, stream) =>
        {
            using (var reader = PooledNetworkReader.Get(stream))
            {
                int sceneIndex = reader.ReadInt32();
                Debug.Log("Message received from server : " + sceneIndex);
                ClientSceneChanged?.Invoke(senderClientId, sceneIndex);
            }

        });
    }
}