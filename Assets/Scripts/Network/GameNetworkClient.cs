using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameNetworkClient : MonoBehaviour
{
    private GameNetworkHub hub;
    void Start()
    {
        hub = GetComponent<GameNetworkHub>();
        hub.NetworkIsReady += OnNetworkReady;
    }
    
    private void OnNetworkReady()
    { 
        if (!hub.NetManager.IsClient)
        {
            enabled = false;
        }
        else
        {
            SceneManager.sceneLoaded += OnSceneLoaded;

        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        hub.ClientToServerSceneChanged(SceneManager.GetActiveScene().buildIndex);
    }
}
