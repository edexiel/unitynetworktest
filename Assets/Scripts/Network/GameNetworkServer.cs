using System.Collections;
using System.Collections.Generic;
using MLAPI.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameNetworkServer : MonoBehaviour
{
    private GameNetworkHub hub;
    void Start()
    {
        hub = GetComponent<GameNetworkHub>();
        hub.NetworkIsReady += OnNetworkReady;
        hub.ClientSceneChanged += OnClientSceneChanged;
    
    }

    private void OnClientSceneChanged(ulong clientId, int sceneIndex)
    {
        Debug.Log("Client "+clientId+" as changed scene to "+SceneManager.GetSceneByBuildIndex(sceneIndex).name);
    }

    private void OnNetworkReady()
    {
        if (!hub.NetManager.IsServer)
        {
            enabled = false;
        }
        else
        {
            NetworkSceneManager.SwitchScene("SampleScene");
        }
    }
    
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
